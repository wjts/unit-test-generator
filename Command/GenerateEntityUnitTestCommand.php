<?php

namespace KCH\Bundle\UnitTests\Generator\EntityBundle\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use KCH\Bundle\UnitTests\Generator\EntityBundle\Model\Property;
use KCH\Bundle\UnitTests\Generator\EntityBundle\Model\PropertyCollection;
use KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\ClassPrototypes;
use KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\FilePrototypes;
use KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes\MethodPrototypes;

/**
 * Class GenerateEntityUnitTestCommand
 * @package UnitTests\Generator\EntityBundle\Command
 */
class GenerateEntityUnitTestCommand extends Command
{
    /**
     * @var array
     */
    private $getterSignatures = ['get', 'is', 'has'];
    /**
     * @var array
     */
    private $setterSignatures = ['set'];

    /**
     *
     */
    protected function configure()
    {
        $this->setName('unit-tests:generate:entity')
            ->setDescription('Generates simple unit test structure for a given entity')
            ->addArgument('entityNamespace', InputArgument::REQUIRED, 'fqn for target entity');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $targetEntityNamespace = $input->getArgument('entityNamespace');
        $targetEntityReflection = new \ReflectionClass($targetEntityNamespace);

        $methodList = $targetEntityReflection->getMethods();

        $skippedCounter = 0;
        $matchedCounter = 0;
        $propertyCollection = new PropertyCollection();
        $unmatchedProperties = [];
        foreach($targetEntityReflection->getProperties() as $reflectionProperty) {
            $getter = $this->findMethod($reflectionProperty->getName(), $this->getterSignatures, $methodList);
            $setter = $this->findMethod($reflectionProperty->getName(), $this->setterSignatures, $methodList);

            if(null === $getter || null === $setter) {
                ++$skippedCounter;
                $unmatchedProperties[] = $reflectionProperty;
                $output->writeln('<comment>Property [' . $reflectionProperty->getName() . '] does not seem to have either standard getter or setter method.</comment>');
            } else {
                ++$matchedCounter;
                $property = new Property($reflectionProperty, $getter, $setter);
                $propertyCollection->add($property);
            }
        }

        $bundleNamespace = $this->bundleNamespace($targetEntityReflection->getNamespaceName());
        $bundleDirectory = $this->bundleDirectory($targetEntityReflection->getFileName());

        $testDirectory = $bundleDirectory . '/Tests/Unit/';
        $testClass = $targetEntityReflection->getShortName() . 'Test';
        $testFilename = $testClass . '.php';

        if(!file_exists($testDirectory)) {
            throw new \RuntimeException(sprintf('Test directory [%s] does not exist.', $testDirectory));
        }

        $testNamespace = $bundleNamespace . '\\Tests\\Unit';

        $filePrototypes = new FilePrototypes();
        $classPrototypes = new ClassPrototypes();
        $methodPrototypes = new MethodPrototypes();

        $file = new \SplFileObject($testDirectory . $testFilename, 'w');
        $file->fwrite($filePrototypes->createOpeningTag());
        $file->fwrite($classPrototypes->createNamespace($testNamespace));
        $file->fwrite($classPrototypes->createUse('PHPUnit\\Framework\\TestCase'));
        $file->fwrite($classPrototypes->createClassSignature($testClass));

        foreach($propertyCollection as $property) {
            $file->fwrite($methodPrototypes->createTestMethodSignature($property->property()->getName(), 'GetterAndSetter'));
            $file->fwrite($methodPrototypes->createDefaultTestMethodBody(
                $targetEntityNamespace,
                $property->setter(),
                $property->getter(),
                $property->setterParameterType()
            ));
            $file->fwrite($methodPrototypes->createTestMethodClosingTag());

            $file->fwrite($methodPrototypes->createTestMethodSignature($property->property()->getName(), 'HasFluentSetter'));
            $file->fwrite($methodPrototypes->createFluentTestMethodBody(
                $targetEntityNamespace,
                $property->setter(),
                $property->setterParameterType()
            ));
            $file->fwrite($methodPrototypes->createTestMethodClosingTag());
        }

        foreach($unmatchedProperties as $property) {
            $file->fwrite($methodPrototypes->createTestMethodSignature($property->getName()));
            $file->fwrite($methodPrototypes->createTestMethodStub());
            $file->fwrite($methodPrototypes->createTestMethodClosingTag());
        }

        $file->fwrite($classPrototypes->createClosingTag());
        $file->fwrite($filePrototypes->createClosingTag());

        $output->writeln('');
        $output->writeln('Summary:');
        $output->writeln('Matched properties: ' . $matchedCounter);
        $output->writeln('Skipped properties: ' . $skippedCounter);
        $output->writeln('');
    }

    /**
     * @param string $propertyName
     * @param [] $methodSignatures
     * @param \ReflectionMethod[] $methods
     * @return null|\ReflectionMethod
     */
    private function findMethod($propertyName, $methodSignatures, $methods)
    {
        foreach($methodSignatures as $methodSignature) {
            $methodName = $methodSignature . ucfirst($propertyName);

            foreach($methods as $method) {
                if(preg_match('/^' . $methodName . '$/', $method->getName())) {
                    return $method;
                }
            }
        }
        // @TODO: zastanowić się nad null object
        return null;
    }

    /**
     * @param $namespace
     * @return string
     */
    private function bundleNamespace($namespace)
    {
        return $this->bundleLocation($namespace, '\\');
    }

    /**
     * @param $path
     * @return string
     */
    private function bundleDirectory($path)
    {
        return $this->bundleLocation($path, DIRECTORY_SEPARATOR);
    }

    /**
     * @param $path
     * @param $separator
     * @return string
     */
    private function bundleLocation($path, $separator)
    {
        $pathParts = explode($separator, $path);
        $pathPartsRev = array_reverse($pathParts);
        $bundlePathParts = [];

        $found = false;
        foreach($pathPartsRev as $pathPart) {
            if(preg_match('/Bundle$/', $pathPart)) {
                $found = true;
            }

            if($found) {
                $bundlePathParts[] = $pathPart;
            }
        }

        return join($separator, array_reverse($bundlePathParts));
    }
}
