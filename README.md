# UnitTests Generator #

## Entity ##

Bundle is used to generate simple unit tests of Entities.

The only thing you need to pass is the Entity namespace:
```bash
$ bin/console unit-tests:generate:entity <entity_namespace>
```

It will create simple unit test with Entity name in _Bundle/Tests/_ directory.

!!! Just remember to escape backslashes !!!

## Installation ##

### For Symfony Framework >= 2.3 ###

Require the bundle and its dependencies with composer:

```bash
$ composer require kch/unittest-generator-entity
```

Register the bundle:

```php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        new KCH\Bundle\UnitTests\Generator\EntityBundle\UnitTestsGeneratorEntityBundle(),
    );
}
```

Enjoy !
