<?php

namespace KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes;


/**
 * Class ClassPrototypes
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes
 */
class ClassPrototypes
{
    /**
     * @param $namespace
     * @return string
     */
    public function createNamespace($namespace)
    {
        $string = sprintf('namespace %s;', $namespace) . PHP_EOL;

        return $string;
    }

    /**
     * @param $use
     * @return string
     */
    public function createUse($use)
    {
        $string = sprintf('use %s;', $use) . PHP_EOL;

        return $string;
    }

    /**
     * @param $className
     * @return string
     */
    public function createClassSignature($className)
    {
        $string = sprintf('class %s extends TestCase {', $className) . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createClosingTag()
    {
        $string = sprintf('}') . PHP_EOL;

        return $string;
    }
}