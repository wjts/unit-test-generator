<?php

namespace KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes;


/**
 * Class MethodPrototypes
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model\UnitTestPrototypes
 */
class MethodPrototypes
{
    /**
     * @param $methodName
     * @return string
     */
    public function createTestMethodSignature($methodName, $suffix = '')
    {
        $string = sprintf('    public function test%s%s()', ucfirst($methodName), $suffix) . PHP_EOL;
        $string .= sprintf('    {') . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createTestMethodClosingTag()
    {
        $string = sprintf('    }') . PHP_EOL;

        return $string;
    }

    /**
     * @param $object
     * @param \ReflectionMethod $setter
     * @param \ReflectionMethod $getter
     * @param $testValue
     * @return string
     */
    public function createDefaultTestMethodBody($object, $setter, $getter, $testValue)
    {
        $string = sprintf('        $object = new \%s();', $object) . PHP_EOL;
        $string .= sprintf('        $testValue = %s;', $testValue) . PHP_EOL;
        $string .= sprintf('        $object->%s($testValue);', $setter->getShortName()) . PHP_EOL;
        $string .= sprintf('        $this->assertEquals($testValue, $object->%s());', $getter->getShortName()) . PHP_EOL;

        return $string;
    }

    public function createFluentTestMethodBody($object, $setter, $testValue)
    {
        $string = sprintf('        $object = new \%s();', $object) . PHP_EOL;
        $string .= sprintf('        $this->assertInstanceOf(\%s::class, $object->%s(%s));', $object, $setter->getShortName(), $testValue) . PHP_EOL;

        return $string;
    }

    /**
     * @return string
     */
    public function createTestMethodStub()
    {
        $string = sprintf('        // @TODO: method stub') . PHP_EOL;

        return $string;
    }
}
