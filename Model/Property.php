<?php

namespace KCH\Bundle\UnitTests\Generator\EntityBundle\Model;


/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
/**
 * Class Property
 * @package KCH\Bundle\UnitTests\Generator\EntityBundle\Model
 */
class Property
{
    /** @var \ReflectionProperty */
    private $property;

    /** @var \ReflectionMethod */
    private $getter;

    /** @var \ReflectionMethod */
    private $setter;

    /**
     * Property constructor.
     * @param $property
     * @param $getter
     * @param $setter
     */
    public function __construct($property, $getter, $setter)
    {
        $this->property = $property;
        $this->getter = $getter;
        $this->setter = $setter;
    }

    /**
     * @return \ReflectionProperty
     */
    public function property()
    {
        return $this->property;
    }

    /**
     * @return \ReflectionMethod
     */
    public function getter()
    {
        return $this->getter;
    }

    /**
     * @return \ReflectionMethod
     */
    public function setter()
    {
        return $this->setter;
    }

    /**
     * @return string
     */
    public function setterParameterType()
    {
        $parameter = $this->setter->getParameters()[0];

        $parameterType = $parameter->getType();

        if ($parameterType->isBuiltin()) {
            switch ($parameterType->getName())
            {
                case 'int':
                    return 123;
                case 'float':
                    return 1.23;
                case 'string':
                    return '"test"';
            }
        }

        $parameterClass = $parameter->getClass();

        if ($parameterClass === null) {
            return '"TEST"';
        }

        switch ($parameterClass->getName()) {
            case 'Money\\Money':
                return sprintf('new \\%s(123, new \\Money\\Currency("PLN"))', $parameterClass->getName());
            default:
                return sprintf('new \\%s()', $parameterClass->getName());
        }
    }
}
